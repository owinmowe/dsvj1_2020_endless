#include "Input.h"

namespace input
{

    Input checkInput()
    {

        Input newInput;

        if (IsKeyPressed(currentKeycodes.enter)) { newInput.enter = true; }
        if (IsKeyPressed(currentKeycodes.pause)) { newInput.pause = true; }
        if (IsKeyPressed(currentKeycodes.jump)) { newInput.jump = true; }
        if (IsKeyPressed(currentKeycodes.dash)) { newInput.dash = true; }

        if (IsKeyDown(currentKeycodes.down_Left)) { newInput.down_Left = true; }
        if (IsKeyDown(currentKeycodes.down_Down)) { newInput.down_Down = true; }
        if (IsKeyDown(currentKeycodes.down_Up)) { newInput.down_Up = true; }
        if (IsKeyDown(currentKeycodes.down_Right)) { newInput.down_Right = true; }

        if (IsKeyPressed(currentKeycodes.pressed_Left)) { newInput.pressed_Left = true; }
        if (IsKeyPressed(currentKeycodes.pressed_Down)) { newInput.pressed_Down = true; }
        if (IsKeyPressed(currentKeycodes.pressed_Up)) { newInput.pressed_Up = true; }


        return newInput;

    }

    void resetKeycodes()
    {

        currentKeycodes.down_Down = DEFAULT_KEYCODES.down_Down;
        currentKeycodes.down_Left = DEFAULT_KEYCODES.down_Left;
        currentKeycodes.down_Right = DEFAULT_KEYCODES.down_Right;
        currentKeycodes.down_Up = DEFAULT_KEYCODES.down_Up;
        currentKeycodes.enter = DEFAULT_KEYCODES.enter;
        currentKeycodes.pause = DEFAULT_KEYCODES.pause;
        currentKeycodes.pressed_Down = DEFAULT_KEYCODES.pressed_Down;
        currentKeycodes.pressed_Left = DEFAULT_KEYCODES.pressed_Left;
        currentKeycodes.dash = DEFAULT_KEYCODES.dash;
        currentKeycodes.pressed_Up = DEFAULT_KEYCODES.pressed_Up;
        currentKeycodes.jump = DEFAULT_KEYCODES.jump;

    }
    bool validateKeycode(int newKeycode, int currentKeycode)
    {
        if (newKeycode == currentKeycode)
        {
            return true;
        }
        else if (newKeycode != 0 &&
            newKeycode != currentKeycodes.down_Down &&
            newKeycode != currentKeycodes.down_Left &&
            newKeycode != currentKeycodes.down_Right &&
            newKeycode != currentKeycodes.down_Up &&
            newKeycode != currentKeycodes.enter &&
            newKeycode != currentKeycodes.pause &&
            newKeycode != currentKeycodes.jump)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    void changeCurrentKeycode(int& keycode)
    {

        int auxKeycode = 0;

        if (IsKeyPressed(KEY_UP))
        {
            auxKeycode = KEY_UP;
        }
        else if (IsKeyPressed(KEY_DOWN))
        {
            auxKeycode = KEY_DOWN;
        }
        else if (IsKeyPressed(KEY_LEFT))
        {
            auxKeycode = KEY_LEFT;
        }
        else if (IsKeyPressed(KEY_RIGHT))
        {
            auxKeycode = KEY_RIGHT;
        }
        else if (IsKeyPressed(KEY_ENTER))
        {
            auxKeycode = KEY_ENTER;
        }
        else if (IsKeyPressed(KEY_SPACE))
        {
            auxKeycode = KEY_SPACE;
        }
        else
        {
            auxKeycode = GetKeyPressed();
            if (auxKeycode != 0)
            {
                auxKeycode -= 32; //GetKeyPressed devuelve solo caracteres, por lo tanto devuelve letras minusculas a diferencia de los Keycodes
            }
        }

        if (validateKeycode(auxKeycode, keycode))
        {
            keycode = auxKeycode;
            currentKeycodes.down_Down = currentKeycodes.pressed_Down;
            currentKeycodes.down_Up = currentKeycodes.pressed_Up;
            currentKeycodes.down_Left = currentKeycodes.pressed_Left;
            currentKeycodes.down_Right = currentKeycodes.dash;
        }

    }

    void updateInputGame()
    {
        currentInput = checkInput();
    }

    newKeycodes currentKeycodes;
    Input currentInput;

}