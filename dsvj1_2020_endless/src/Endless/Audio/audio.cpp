#include "audio.h"
#include <string>

namespace audio
{

    float musicVolume = 0.3f;
    float soundVolume = 0.1f;

    const int MAX_SIZE_FILEPATH = 100;
    const int MAX_MUSIC_FILES = 2;
    const int MAX_SOUND_FILES = 10;

    struct SoundFile
    {
        Sound sound;
        char FILE_PATH[MAX_SIZE_FILEPATH];
    };

    struct MusicFile
    {
        Music music;
        char FILE_PATH[MAX_SIZE_FILEPATH];
    };

    std::vector<MusicFile> musicFiles (MAX_MUSIC_FILES);
    enum class MUSIC_ID{GAMEMUSIC_ID, MENUMUSIC_ID};

    std::vector<SoundFile> soundFiles (MAX_SOUND_FILES);
    enum class SOUND_ID{JUMP_ID, DASH_ID, HIT_ID, MOVE_ID, ROCK_BREAK_ID, ITEM_GET_ID};

    void setMusicVector()
    {
        std::string gameMusicString = "res/assets/audio/music/GameMusic.mp3";
        gameMusicString.copy(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].FILE_PATH, gameMusicString.size() + 1);
        std::string menuMusicString = "res/assets/audio/music/MenuMusic.mp3";
        menuMusicString.copy(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].FILE_PATH, menuMusicString.size() + 1);
    }

    void setSoundVector()
    {
        std::string moveString = "res/assets/audio/sounds/MoveSound.wav";
        moveString.copy(soundFiles[static_cast<int>(SOUND_ID::MOVE_ID)].FILE_PATH, moveString.size() + 1);
        std::string hitString = "res/assets/audio/sounds/HitSound.wav";
        hitString.copy(soundFiles[static_cast<int>(SOUND_ID::HIT_ID)].FILE_PATH, hitString.size() + 1);
        std::string rockBreakString = "res/assets/audio/sounds/RockBreakSound.wav";
        rockBreakString.copy(soundFiles[static_cast<int>(SOUND_ID::ROCK_BREAK_ID)].FILE_PATH, rockBreakString.size() + 1);
        std::string jumpString = "res/assets/audio/sounds/JumpSound.wav";
        jumpString.copy(soundFiles[static_cast<int>(SOUND_ID::JUMP_ID)].FILE_PATH, jumpString.size() + 1);
        std::string dashString = "res/assets/audio/sounds/DashSound.wav";
        dashString.copy(soundFiles[static_cast<int>(SOUND_ID::DASH_ID)].FILE_PATH, dashString.size() + 1);
        std::string itemGetString = "res/assets/audio/sounds/ItemGet.wav";
        itemGetString.copy(soundFiles[static_cast<int>(SOUND_ID::ITEM_GET_ID)].FILE_PATH, itemGetString.size() + 1);
    }

    void setAllSoundVolume()
    {
        for (int i = 0; i < static_cast<int>(soundFiles.size()); i++)
        {
            SetSoundVolume(soundFiles[i].sound, musicVolume);
        }
    }

    void setAllMusicVolume()
    {
        for (int i = 0; i < static_cast<int>(musicFiles.size()); i++)
        {
            SetMusicVolume(musicFiles[i].music, musicVolume);
        }
    }

    void loadSoundsInMemory()
    {
        setSoundVector();
        for (int i = 0; i < static_cast<int>(soundFiles.size()); i++)
        {
            soundFiles[i].sound = LoadSound(soundFiles[i].FILE_PATH);
        }
    }

    void unloadSoundsFromMemory()
    {
        for (int i = 0; i < static_cast<int>(soundFiles.size()); i++)
        {
            UnloadSound(soundFiles[i].sound);
        }
    }

    void loadMusicInMemory()
    {
        setMusicVector();
        for (int i = 0; i < static_cast<int>(musicFiles.size()); i++)
        {
            musicFiles[i].music = LoadMusicStream(musicFiles[i].FILE_PATH);
        }
    }

    void unloadMusicFromMemory()
    {
        for (int i = 0; i < static_cast<int>(musicFiles.size()); i++)
        {
            UnloadMusicStream(musicFiles[i].music);
        }
    }

    void menuAudioUpdate()
    {
        UpdateMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::MOVE_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MOVE_ID)].sound); }
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::SELECT_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::JUMP_ID)].sound); }
    }
    void menuAudioStart()
    {
        PlayMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
    }

    void gameAudioUpdate()
    {
        UpdateMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::MOVE)){ PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MOVE_ID)].sound); }
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::HIT_ID)].sound); }
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::ROCK_BREAK)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ROCK_BREAK_ID)].sound); }
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::JUMP)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::JUMP_ID)].sound); }
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::DASH)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::DASH_ID)].sound); }
        if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::ITEM_GET)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ITEM_GET_ID)].sound); }
    }
    void gameAudioStart()
    {
        PlayMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
    }
}