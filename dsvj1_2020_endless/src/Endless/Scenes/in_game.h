#ifndef IN_GAME_H
#define IN_GAME_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Endless/Audio/audio.h"
#include "Endless/Draw/textures.h"
#include "Endless/Input/input.h"
#include "Endless/EventSystem/event_system.h"
#include "Endless/Background/background.h"
#include "Endless/Ui/ui.h"
#include "Endless/GameObject/player.h"
#include "Endless/GameObject/Enemies/jump_object.h"
#include "Endless/GameObject/Enemies/dash_object.h"
#include "Endless/GameObject/Enemies/evade_object.h"
#include "Endless/GameObject/Items/extraPoints.h"
#include "Endless/Collision/Collision.h"
#include "Endless/Draw/post_processing.h"

namespace in_game
{
	const float FRONT_BG_SPEED = .80f;
	const float MIDDLE_BG_SPEED = .25f;
	const float BACK_BG_SPEED = .10f;
	const float DAY_NIGHT_BG_SPEED = .05f;
	const int MAX_ENTITIES_FOR_TYPES = 20;
	const float STARTING_TIME_FOR_SPAWN_ENEMY = 4;
	const float SPAWN_ACELERATION = .05f;
	const float SPAWN_MAX_SPEED = .5f;
	const Vector2 SCORE_POSITION_PLAYING = {.5f, .15f};
	const int SCORE_SIZE_PLAYING = 30;
	const Vector2 SCORE_POSITION_END = { .5f, .5f };
	const int SCORE_SIZE_END = 60;
	const Vector2 SCORE_POSITION_PAUSE = { .5f, .5f };
	const int SCORE_SIZE_PAUSE = 60;
	const int TUTORIAL_1_TEXT_SIZE = 50;
	const Vector2 TUTORIAL_1_TEXT_POSITION = { .5f, .2f };
	const int TUTORIAL_2_TEXT_SIZE = 50;
	const Vector2 TUTORIAL_2_TEXT_POSITION = { .5f, .2f };
	const int TUTORIAL_3_TEXT_SIZE = 40;
	const Vector2 TUTORIAL_3_TEXT_POSITION = { .5f, .2f };
	const int HIGH_SCORE_STORAGE_POSITION = 1;
#if DEBUG
	const Color DEBUG_COLOR = GREEN;
	const int DEBUG_LINES_SIZE = 3;
#endif
	enum class GAMESTATE{ TUTORIAL_0, TUTORIAL_1, TUTORIAL_2, TUTORIAL_3, PLAYING, PAUSED, ENDED};

	class InGame : public SceneBase
	{

	private:
		void init() override;
		void setGameObjects();
		void setBackgrounds();
		void enemySpawnLogic();
		void pauseCommands();
		void updateObjects();
		void directGameplayCommands();
		void updateBGs();
		void draw() override;
		void drawGameObjects();
		void drawBackBGs();
		game_objects::Player* player;
		game_objects::JumpObject* jumpObjects[MAX_ENTITIES_FOR_TYPES];
		game_objects::DashObject* dashObjects[MAX_ENTITIES_FOR_TYPES];
		game_objects::EvadeObject* evadeObjects[MAX_ENTITIES_FOR_TYPES];
		game_objects::ExtraPointsItem* extraPointsItems[MAX_ENTITIES_FOR_TYPES];
		float maxTimeForEnemy = STARTING_TIME_FOR_SPAWN_ENEMY;
		float timeFromLastEnemy = 0;
		Background* frontBG;
		Background* middleBG;
		Background* backBG;
		Background* dayNightBG;
		int currentScore = 0;
		GAMESTATE currentGamestate;
		bool newHighScore = false;
		bool tutorialTextOn = false;

	public:
		InGame();
		~InGame();
		void deInit() override;
		void update() override;
		void gameEndedCommands();
	};
}

#endif

