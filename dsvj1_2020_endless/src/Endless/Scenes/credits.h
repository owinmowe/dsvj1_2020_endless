#ifndef CREDITS_H
#define CREDITS_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Endless/Audio/audio.h"
#include "Endless/Draw/textures.h"
#include "Endless/Ui/ui.h"

namespace credits
{

	using namespace ui;

	const char TITLE_TEXT[] = "CREDITS";
	const float TITLE_SIZE = 100;
	const float TITLE_POSITION = 1 / 8.f;

	const int GUI_SIZE = 1;
	const float BUTTON_WIDTH = 1 / 6.f;
	const float BUTTON_HEIGHT = 1 / 10.f;
	const float GUI_STARTING_POSITION = 7 / 8.f;
	const float GUI_SEPARATION = 1 / 8.f;

	class Credits : public SceneBase
	{

	private:
		void init() override;
		GUIcomponent* GUIComponent[GUI_SIZE];

	public:
		Credits();
		~Credits();
		void deInit() override;
		void update() override;
		void draw() override;

	};

	class BackButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Back";
	};

}

#endif