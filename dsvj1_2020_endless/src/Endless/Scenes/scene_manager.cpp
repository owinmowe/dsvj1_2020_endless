#include "scene_manager.h"

namespace sceneConfig
{

	bool playing = true;
	bool pause = false;
	SceneBase* currentScene_Ptr;

	void ChangeSceneTo(sceneConfig::Scene nextScene)
	{
		if(currentScene_Ptr != nullptr)
		{
			currentScene_Ptr->deInit();
		}
		delete currentScene_Ptr;

		switch (nextScene)
		{
		case sceneConfig::Scene::MAIN_MENU:
			currentScene_Ptr = new main_menu::MainMenu;
			break;
		case sceneConfig::Scene::OPTIONS:
			currentScene_Ptr = new options::Options;
			break;
		case sceneConfig::Scene::CREDITS:
			currentScene_Ptr = new credits::Credits;
			break;
		case sceneConfig::Scene::IN_GAME:
			currentScene_Ptr = new in_game::InGame;
			break;
		default:
			break;
		}

	}
}