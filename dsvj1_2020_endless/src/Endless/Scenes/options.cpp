#include "options.h"

namespace options
{
	Options::Options()
	{
		init();
	}
	Options::~Options()
	{
		deInit();
	}
	void Options::init()
	{
		buttonSize.x = BUTTON_WIDTH;
		buttonSize.y = BUTTON_HEIGHT;
		SliderSize.x = SLIDER_WIDTH;
		SliderSize.y = SLIDER_HEIGHT;
		setGUITexturesSize();

		GUIComponent[0] = new SoundVolumeSlider;
		GUIComponent[1] = new MusicVolumeSlider;
		GUIComponent[2] = new FullScreenButton;
		GUIComponent[3] = new BackButton;

		GUIComponent[0]->selected = true;
	}
	void Options::deInit()
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUIComponent[i];
		}
	}
	void Options::update()
	{
		audio::menuAudioUpdate();
		upDownGUILogic(GUI_SIZE, GUIComponent);
	}
	void Options::draw()
	{
		DrawTexture(textures::files::uiTextureFiles[static_cast<int>(textures::files::UI_TEXTURES_ID::GENERAL_BG)].texture, 0, 0, WHITE);
		drawTextWithSecondFont(TITLE_TEXT, .5f, TITLE_POSITION, TITLE_SIZE, BLACK);
		drawUpDownGUI(GUIComponent, GUI_SIZE, GUI_STARTING_POSITION, GUI_SEPARATION);
	}

	SoundVolumeSlider::SoundVolumeSlider()
	{
		handlePosition = static_cast<int>(audio::soundVolume * 10);
	}
	std::string SoundVolumeSlider::getText()
	{
		return derivedText;
	}
	void SoundVolumeSlider::leftAction()
	{
		audio::soundVolume -= .1f;
		audio::setAllSoundVolume();
	}
	void SoundVolumeSlider::rightAction()
	{
		audio::soundVolume += .1f;
		audio::setAllSoundVolume();
	}
	MusicVolumeSlider::MusicVolumeSlider()
	{
		handlePosition = static_cast<int>(audio::musicVolume * 10);
	}
	std::string MusicVolumeSlider::getText()
	{
		return derivedText;
	}
	void MusicVolumeSlider::leftAction()
	{
		audio::musicVolume -= .1f;
		audio::setAllMusicVolume();
	}
	void MusicVolumeSlider::rightAction()
	{
		audio::musicVolume += .1f;
		audio::setAllMusicVolume();
	}
	std::string FullScreenButton::getText()
	{
		std::string aux;
		IsWindowFullscreen() ? aux = derivedTextFullScreen : aux = derivedTextWindow;
		return aux;
	}
	void FullScreenButton::action()
	{
		ToggleFullscreen();
	}
	std::string BackButton::getText()
	{
		return derivedText;
	}
	void BackButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}
}