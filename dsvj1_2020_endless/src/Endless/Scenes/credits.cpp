#include "credits.h"

namespace credits
{
	Credits::Credits()
	{
		init();
	}
	Credits::~Credits()
	{
		deInit();
	}
	void Credits::init()
	{
		GUIComponent[0] = new BackButton;
		GUIComponent[0]->selected = true;
	}
	void Credits::deInit()
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUIComponent[i];
		}
	}
	void Credits::update()
	{
		audio::menuAudioUpdate();
		upDownGUILogic(GUI_SIZE, GUIComponent);
	}
	void Credits::draw()
	{
		DrawTexture(textures::files::uiTextureFiles[static_cast<int>(textures::files::UI_TEXTURES_ID::GENERAL_BG)].texture, 0, 0, WHITE);
		drawTextWithSecondFont(TITLE_TEXT, .5f, TITLE_POSITION, TITLE_SIZE, BLACK);
		ui::drawTextWithSecondFont("Made by Adrian Sgro with raylib library.", .5f, .25f, 40, BLACK);
		ui::drawTextWithSecondFont("Sounds were made with Bosca Ceoil or are free domain and downloaded from freesound.org", .5f, .35f, 40, BLACK);
		ui::drawTextWithSecondFont("Menu music is 'Horizon Flare' by Alexander Nakarada (Free domain).", .5f, .45f, 40, BLACK);
		ui::drawTextWithSecondFont("Game music is 'Brother Unite' by Alexander Nakarada (Free domain).", .5f, .55f, 40, BLACK);
		ui::drawTextWithSecondFont("Enviroment Textures made by Adrian Sgro .", .5f, .65f, 40, BLACK);
		ui::drawTextWithSecondFont("Character Textures made by Francisco Rodriguez.", .5f, .75f, 40, BLACK);
		drawUpDownGUI(GUIComponent, GUI_SIZE, GUI_STARTING_POSITION, GUI_SEPARATION);
	}

	std::string BackButton::getText()
	{
		return derivedText;
	}
	void BackButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}
}