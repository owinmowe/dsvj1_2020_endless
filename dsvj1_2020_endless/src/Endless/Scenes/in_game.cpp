#include "in_game.h"

namespace in_game
{
	InGame::InGame()
	{
		init();
	}
	InGame::~InGame()
	{
		deInit();
	}
	void InGame::init()
	{
		currentGamestate = GAMESTATE::TUTORIAL_0;
		newHighScore = false;
		tutorialTextOn = false;
		maxTimeForEnemy = STARTING_TIME_FOR_SPAWN_ENEMY;
		timeFromLastEnemy = 0;
		currentScore = 0;
		global_ilumination::resetIllumination();
		textures::LoadGameTextures();
		setGameObjects();
		setBackgrounds();
		audio::gameAudioStart();
	}
	void InGame::setGameObjects()
	{
		using namespace textures::files;
		using namespace game_objects;
		player = new Player(&gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::PLAYER)].texture, { 0, 0, 0, 0 }, LANE_POSITION::MIDDLE);
		for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
		{
			jumpObjects[i] = new JumpObject(&gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::JUMP_OBSTACLE)].texture, { 0, 0, 0, 0 }, LANE_POSITION::UP);
			dashObjects[i] = new DashObject(&gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::DASH_OBSTACLE)].texture, { 0, 0, 0, 0 }, LANE_POSITION::UP);
			evadeObjects[i] = new EvadeObject(&gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EVADE_OBSTACLE)].texture, { 0, 0, 0, 0 }, LANE_POSITION::UP);
			extraPointsItems[i] = new ExtraPointsItem(&gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EXTRA_POINTS_RUN_ITEM)].texture,
				&gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EXTRA_POINTS_DASH_ITEM)].texture, 
				&gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EXTRA_POINTS_JUMP_ITEM)].texture, 
				{ 0, 0, 0, 0 }, LANE_POSITION::UP);
		}
		extraPointsItems[0]->activate(MIDDLE_BG_SPEED, game_objects::EXTRA_POINTS_TYPE::RUN);
		extraPointsItems[0]->setLanePosition(LANE_POSITION::UP);
		extraPointsItems[0]->resetPositionX();
	}
	void InGame::setBackgrounds()
	{
		using namespace textures::files;
		using namespace game_objects;
		frontBG = new Background(&backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::FRONT_BG)].texture, { 0,0 }, FRONT_BG_SPEED);
		middleBG = new Background(&backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::MIDDLE_BG)].texture, { 0,0 }, MIDDLE_BG_SPEED);
		backBG = new Background(&backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::BACK_BG)].texture, { 0,0 }, BACK_BG_SPEED);
		dayNightBG = new Background(&backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::DAY_NIGHT_BG)].texture, { 0,0 }, DAY_NIGHT_BG_SPEED);
	}
	void InGame::deInit()
	{
		textures::UnloadGameTextures();
		delete frontBG;
		delete middleBG;
		delete backBG;
		delete dayNightBG;
		delete player;
		for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
		{
			delete jumpObjects[i];
			delete evadeObjects[i];
			delete dashObjects[i];
			delete extraPointsItems[i];
		}
	}
	void InGame::update()
	{

		switch (currentGamestate)
		{
		case in_game::GAMESTATE::TUTORIAL_0:
			if (player->getPosition().x + player->getRectangle().width > extraPointsItems[0]->getPosition().x)
			{
				if (input::currentInput.pressed_Up)
				{
					player->moveUp();
					evadeObjects[0]->activate(MIDDLE_BG_SPEED);
					evadeObjects[0]->setLanePosition(player->getLanePosition());
					evadeObjects[0]->resetPositionX();
					currentGamestate = GAMESTATE::TUTORIAL_1;
					tutorialTextOn = false;
				}
				else
				{
					tutorialTextOn = true;
				}
			}
			else
			{
				if (input::currentInput.enter) { currentGamestate = GAMESTATE::PLAYING; }
				updateObjects();
				updateBGs();
			}
			break;
		case in_game::GAMESTATE::TUTORIAL_1:
			if(player->getPosition().x + player->getRectangle().width > evadeObjects[0]->getPosition().x)
			{
				if (input::currentInput.pressed_Down)
				{
					player->moveDown();
					jumpObjects[0]->activate(MIDDLE_BG_SPEED);
					jumpObjects[0]->setLanePosition(player->getLanePosition());
					jumpObjects[0]->resetPositionX();
					extraPointsItems[0]->activate(MIDDLE_BG_SPEED, game_objects::EXTRA_POINTS_TYPE::JUMP, jumpObjects[0]->getLanePosition(), true);
					currentGamestate = GAMESTATE::TUTORIAL_2;
					tutorialTextOn = false;
				}
				else
				{
					tutorialTextOn = true;
				}
			}
			else
			{
				if (input::currentInput.enter) { currentGamestate = GAMESTATE::PLAYING; }
				updateObjects();
				updateBGs();
			}
			break;
		case in_game::GAMESTATE::TUTORIAL_2:
			if (player->getPosition().x + player->getRectangle().width > jumpObjects[0]->getPosition().x)
			{
				if (input::currentInput.jump)
				{
					player->jump();
					dashObjects[0]->activate(MIDDLE_BG_SPEED);
					dashObjects[0]->setLanePosition(player->getLanePosition());
					dashObjects[0]->resetPositionX();
					extraPointsItems[1]->activate(MIDDLE_BG_SPEED, game_objects::EXTRA_POINTS_TYPE::DASH, dashObjects[0]->getLanePosition(), false);
					currentGamestate = GAMESTATE::TUTORIAL_3;
					tutorialTextOn = false;
				}
				else
				{
					tutorialTextOn = true;
				}
			}
			else
			{
				if (input::currentInput.enter) { currentGamestate = GAMESTATE::PLAYING; }
				updateObjects();
				updateBGs();
			}
			break;
		case in_game::GAMESTATE::TUTORIAL_3:
			if (player->getPosition().x + player->getRectangle().width > dashObjects[0]->getPosition().x)
			{
				if (input::currentInput.dash)
				{
					player->dash();
					currentGamestate = GAMESTATE::PLAYING;
					tutorialTextOn = false;
				}
				else
				{
					tutorialTextOn = true;
				}
			}
			else
			{
				if(input::currentInput.enter){ currentGamestate = GAMESTATE::PLAYING; }
				updateObjects();
				updateBGs();
			}
			break;
		case in_game::GAMESTATE::PLAYING:
			currentScore++;
			global_ilumination::updateIlumination();
			directGameplayCommands();
			enemySpawnLogic();
			updateObjects();
			updateBGs();
			break;
		case in_game::GAMESTATE::PAUSED:
			pauseCommands();
			break;
		case in_game::GAMESTATE::ENDED:
			gameEndedCommands();
			break;
		default:
			break;
		}
		audio::gameAudioUpdate();
	}
	void InGame::gameEndedCommands()
	{
		if (input::currentInput.jump)
		{
			sceneConfig::ChangeSceneTo(sceneConfig::Scene::IN_GAME);
		}
		else if (input::currentInput.enter)
		{
			sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
		}
	}
	void InGame::enemySpawnLogic()
	{
		timeFromLastEnemy += GetFrameTime();
		if(timeFromLastEnemy > maxTimeForEnemy)
		{
			int enemyType = GetRandomValue(1, 4);
			switch (enemyType)
			{
			case 1:
				for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
				{
					if (!dashObjects[i]->isActive())
					{
						dashObjects[i]->activate(MIDDLE_BG_SPEED);
						for (int j = 0; j < MAX_ENTITIES_FOR_TYPES; j++)
						{
							if (!extraPointsItems[j]->isActive())
							{
								extraPointsItems[j]->activate(MIDDLE_BG_SPEED, game_objects::EXTRA_POINTS_TYPE::DASH, dashObjects[i]->getLanePosition(), false);
								break;
							}
						}
						break;
					}
				}
				maxTimeForEnemy -= SPAWN_ACELERATION;
				if (maxTimeForEnemy < SPAWN_MAX_SPEED) { maxTimeForEnemy = SPAWN_MAX_SPEED; }
				timeFromLastEnemy = 0;
				break;
			case 2:
				for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
				{
					if (!jumpObjects[i]->isActive())
					{
						jumpObjects[i]->activate(MIDDLE_BG_SPEED);
						for (int j = 0; j < MAX_ENTITIES_FOR_TYPES; j++)
						{
							if (!extraPointsItems[j]->isActive())
							{
								extraPointsItems[j]->activate(MIDDLE_BG_SPEED, game_objects::EXTRA_POINTS_TYPE::JUMP, jumpObjects[i]->getLanePosition(), true);
								break;
							}
						}
						break;
					}
				}
				maxTimeForEnemy -= SPAWN_ACELERATION;
				if (maxTimeForEnemy < SPAWN_MAX_SPEED) { maxTimeForEnemy = SPAWN_MAX_SPEED; }
				timeFromLastEnemy = 0;
				break;
			case 3:
				for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
				{
					if (!evadeObjects[i]->isActive())
					{
						evadeObjects[i]->activate(MIDDLE_BG_SPEED);
						break;
					}
				}
				maxTimeForEnemy -= SPAWN_ACELERATION;
				if (maxTimeForEnemy < SPAWN_MAX_SPEED) { maxTimeForEnemy = SPAWN_MAX_SPEED; }
				timeFromLastEnemy = 0;
				break;
			case 4:
				for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
				{
					if (!extraPointsItems[i]->isActive())
					{
						extraPointsItems[i]->activate(MIDDLE_BG_SPEED, game_objects::EXTRA_POINTS_TYPE::RUN);
						break;
					}
				}
				maxTimeForEnemy -= SPAWN_ACELERATION;
				if (maxTimeForEnemy < SPAWN_MAX_SPEED) { maxTimeForEnemy = SPAWN_MAX_SPEED; }
				timeFromLastEnemy = 0;
				break;
			default:
				break;
			}
		}
	}
	void InGame::updateObjects()
	{
		player->update();
		for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
		{
			if (dashObjects[i]->isActive()) { dashObjects[i]->update(); }
			if (jumpObjects[i]->isActive()) { jumpObjects[i]->update(); }
			if (evadeObjects[i]->isActive()) { evadeObjects[i]->update(); }
			if (extraPointsItems[i]->isActive()) { extraPointsItems[i]->update(); }
		}
		if(collision::playerCollided(player, dashObjects, jumpObjects, evadeObjects, MAX_ENTITIES_FOR_TYPES))
		{
			event_system::externalEventSystem->setEvent(event_system::EVENT_ID::HIT);
			currentGamestate = GAMESTATE::ENDED;
			if(currentScore > LoadStorageValue(HIGH_SCORE_STORAGE_POSITION))
			{
				SaveStorageValue(HIGH_SCORE_STORAGE_POSITION, currentScore);
				newHighScore = true;
			}
		}
		if(collision::playerItemGot(player, currentScore, extraPointsItems, MAX_ENTITIES_FOR_TYPES))
		{
			event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ITEM_GET);
		}
	}
	void InGame::directGameplayCommands()
	{
		if (input::currentInput.pressed_Down)
		{
			player->moveDown();
		}
		else if (input::currentInput.pressed_Up)
		{
			player->moveUp();
		}
		else if (input::currentInput.jump)
		{
			player->jump();
		}
		else if (input::currentInput.dash)
		{
			player->dash();
		}
		if (input::currentInput.pause)
		{
			currentGamestate = GAMESTATE::PAUSED;
		}
	}
	void InGame::updateBGs()
	{
		dayNightBG->update();
		backBG->update();
		middleBG->update();
		frontBG->update();
	}
	void InGame::pauseCommands()
	{
		if (input::currentInput.pause)
		{
			currentGamestate = GAMESTATE::PLAYING;
		}
		else if (input::currentInput.enter)
		{
			sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
		}
	}
	void InGame::draw()
	{
		drawBackBGs();
		drawGameObjects();
		frontBG->draw();
#if DEBUG
		DrawRectangleLinesEx(player->getCollider(), DEBUG_LINES_SIZE, DEBUG_COLOR);
		for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
		{
			if (dashObjects[i]->isActive()) { DrawRectangleLinesEx(dashObjects[i]->getCollider(), DEBUG_LINES_SIZE, DEBUG_COLOR); }
			if (jumpObjects[i]->isActive()) { DrawRectangleLinesEx(jumpObjects[i]->getCollider(), DEBUG_LINES_SIZE, DEBUG_COLOR); }
			if (evadeObjects[i]->isActive()) { DrawRectangleLinesEx(evadeObjects[i]->getCollider(), DEBUG_LINES_SIZE, DEBUG_COLOR); }
			if (extraPointsItems[i]->isActive()) { DrawRectangleLinesEx(extraPointsItems[i]->getCollider(), DEBUG_LINES_SIZE, DEBUG_COLOR); }
		}
#endif

		switch (currentGamestate)
		{
		case in_game::GAMESTATE::TUTORIAL_0:
			if (tutorialTextOn)
			{
				ui::drawTextWithSecondFont(FormatText("If hermes bless you, you can get extra hermes favor on the ground."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y, TUTORIAL_1_TEXT_SIZE, BLACK);
				ui::drawTextWithSecondFont(FormatText("Press W to go up a lane and grab the item worth 200 favor."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y + 0.1f, TUTORIAL_1_TEXT_SIZE, BLACK);
			}
			else
			{
				ui::drawTextWithSecondFont(FormatText("Press Enter to skip tutorial."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y, TUTORIAL_1_TEXT_SIZE, BLACK);
			}
			break;
		case in_game::GAMESTATE::TUTORIAL_1:
			if(tutorialTextOn)
			{
				ui::drawTextWithSecondFont(FormatText("Trees. You need to evade them."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y, TUTORIAL_1_TEXT_SIZE, BLACK);
				ui::drawTextWithSecondFont(FormatText("Press S to go Down a lane and evade the tree."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y + 0.1f, TUTORIAL_1_TEXT_SIZE, BLACK);
			}
			else
			{
				ui::drawTextWithSecondFont(FormatText("Press Enter to skip tutorial."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y, TUTORIAL_1_TEXT_SIZE, BLACK);
			}
			break;
		case in_game::GAMESTATE::TUTORIAL_2:
			if(tutorialTextOn)
			{
				ui::drawTextWithSecondFont(FormatText("Fallen logs. You can evade them but by doing a feat of strenght"), TUTORIAL_2_TEXT_POSITION.x, TUTORIAL_2_TEXT_POSITION.y, TUTORIAL_2_TEXT_SIZE, BLACK);
				ui::drawTextWithSecondFont(FormatText("like jumping over them you can get extra Hermes favor."), TUTORIAL_2_TEXT_POSITION.x, TUTORIAL_2_TEXT_POSITION.y + 0.1f, TUTORIAL_2_TEXT_SIZE, BLACK);
				ui::drawTextWithSecondFont(FormatText("Press the Space key to jump and grab the item worth 400 favor."), TUTORIAL_2_TEXT_POSITION.x, TUTORIAL_2_TEXT_POSITION.y + 0.2f, TUTORIAL_2_TEXT_SIZE, BLACK);
			}
			else
			{
				ui::drawTextWithSecondFont(FormatText("Press Enter to skip tutorial."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y, TUTORIAL_1_TEXT_SIZE, BLACK);
			}
			break;
		case in_game::GAMESTATE::TUTORIAL_3:
			if(tutorialTextOn)
			{
				ui::drawTextWithSecondFont(FormatText("Rocks. They look fragile, by dashing through you will get more Hermes favor."), TUTORIAL_3_TEXT_POSITION.x, TUTORIAL_3_TEXT_POSITION.y, TUTORIAL_3_TEXT_SIZE, BLACK);
				ui::drawTextWithSecondFont(FormatText("Press the D key to dash and grab the item worth 300 favor."), TUTORIAL_3_TEXT_POSITION.x, TUTORIAL_3_TEXT_POSITION.y + 0.1f, TUTORIAL_3_TEXT_SIZE, BLACK);
			}
			else
			{
				ui::drawTextWithSecondFont(FormatText("Press Enter to skip tutorial."), TUTORIAL_1_TEXT_POSITION.x, TUTORIAL_1_TEXT_POSITION.y, TUTORIAL_1_TEXT_SIZE, BLACK);
			}
			break;
		case in_game::GAMESTATE::PLAYING:
			ui::drawTextWithSecondFont(FormatText("Hermes favor: %08i", currentScore), SCORE_POSITION_PLAYING.x, SCORE_POSITION_PLAYING.y, SCORE_SIZE_PLAYING, BLACK);
			break;
		case in_game::GAMESTATE::PAUSED:
			if (screenConfig::currentFrame / 30 % 2 == 0)
			{
				ui::drawTextWithSecondFont(FormatText("Press P to Unpause"), SCORE_POSITION_PAUSE.x, SCORE_POSITION_PAUSE.y - 0.1f, SCORE_SIZE_PAUSE, BLACK);
			}
			ui::drawTextWithSecondFont(FormatText("Hermes favor: %08i", currentScore), SCORE_POSITION_PAUSE.x, SCORE_POSITION_PAUSE.y, SCORE_SIZE_PAUSE, BLACK);
			ui::drawTextWithSecondFont(FormatText("Press enter to get back to the main menu"), SCORE_POSITION_PAUSE.x, SCORE_POSITION_PAUSE.y + 0.1f, SCORE_SIZE_PAUSE, BLACK);
			break;
		case in_game::GAMESTATE::ENDED:
			DrawRectangle(0, 0, screenConfig::SCREEN_WIDTH, screenConfig::SCREEN_HEIGHT, BLACK);
			if (newHighScore)
			{
				ui::drawTextWithSecondFont(FormatText("Hermes favor: %08i", currentScore), SCORE_POSITION_END.x, SCORE_POSITION_END.y - 0.2f, SCORE_SIZE_END, WHITE);
				ui::drawTextWithSecondFont(FormatText("New succesor!", currentScore), SCORE_POSITION_END.x, SCORE_POSITION_END.y - 0.1f, SCORE_SIZE_END, WHITE);
			}
			else
			{
				ui::drawTextWithSecondFont(FormatText("Hermes favor: %08i", currentScore), SCORE_POSITION_END.x, SCORE_POSITION_END.y - 0.1f, SCORE_SIZE_END, WHITE);
			}
			ui::drawTextWithSecondFont(FormatText("Current successor highest favor: %08i", LoadStorageValue(HIGH_SCORE_STORAGE_POSITION)), SCORE_POSITION_END.x, SCORE_POSITION_END.y, SCORE_SIZE_END, WHITE);
			ui::drawTextWithSecondFont(FormatText("Press enter to get back to the main menu"), SCORE_POSITION_PAUSE.x, SCORE_POSITION_PAUSE.y + 0.1f, SCORE_SIZE_PAUSE, WHITE);
			ui::drawTextWithSecondFont(FormatText("Press space to try again"), SCORE_POSITION_PAUSE.x, SCORE_POSITION_PAUSE.y + 0.2f, SCORE_SIZE_PAUSE, WHITE);
		default:
			break;
		}
	}
	void InGame::drawGameObjects()
	{
		using namespace game_objects;
		switch (player->getLanePosition())
		{
		case game_objects::LANE_POSITION::UP:
			player->draw();
			for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
			{
				if (dashObjects[i]->isActive()) { dashObjects[i]->draw(); }
				if (jumpObjects[i]->isActive()) { jumpObjects[i]->draw(); }
				if (evadeObjects[i]->isActive()) { evadeObjects[i]->draw(); }
				if (extraPointsItems[i]->isActive()) { extraPointsItems[i]->draw(); }
			}
			break;
		case game_objects::LANE_POSITION::MIDDLE:
			for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
			{
				if (dashObjects[i]->isActive() && (dashObjects[i]->getLanePosition() == LANE_POSITION::UP || dashObjects[i]->getLanePosition() == LANE_POSITION::MIDDLE)) { dashObjects[i]->draw(); }
				if (jumpObjects[i]->isActive() && (jumpObjects[i]->getLanePosition() == LANE_POSITION::UP || jumpObjects[i]->getLanePosition() == LANE_POSITION::MIDDLE)) { jumpObjects[i]->draw(); }
				if (evadeObjects[i]->isActive() && (evadeObjects[i]->getLanePosition() == LANE_POSITION::UP || evadeObjects[i]->getLanePosition() == LANE_POSITION::MIDDLE)) { evadeObjects[i]->draw(); }
				if (extraPointsItems[i]->isActive() && (extraPointsItems[i]->getLanePosition() == LANE_POSITION::UP || extraPointsItems[i]->getLanePosition() == LANE_POSITION::MIDDLE)) { extraPointsItems[i]->draw(); }
			}
			player->draw();
			for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
			{
				if (dashObjects[i]->isActive() && dashObjects[i]->getLanePosition() == LANE_POSITION::DOWN) { dashObjects[i]->draw(); }
				if (jumpObjects[i]->isActive() && jumpObjects[i]->getLanePosition() == LANE_POSITION::DOWN) { jumpObjects[i]->draw(); }
				if (evadeObjects[i]->isActive() && evadeObjects[i]->getLanePosition() == LANE_POSITION::DOWN) { evadeObjects[i]->draw(); }
				if (extraPointsItems[i]->isActive() && extraPointsItems[i]->getLanePosition() == LANE_POSITION::DOWN) { extraPointsItems[i]->draw(); }
			}
			break;
		case game_objects::LANE_POSITION::DOWN:
			for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
			{
				if (dashObjects[i]->isActive()) { dashObjects[i]->draw(); }
				if (jumpObjects[i]->isActive()) { jumpObjects[i]->draw(); }
				if (evadeObjects[i]->isActive()) { evadeObjects[i]->draw(); }
				if (extraPointsItems[i]->isActive()) { extraPointsItems[i]->draw(); }
			}
			player->draw();
			break;
		default:
			break;
		}
	}
	void InGame::drawBackBGs()
	{
		dayNightBG->draw();
		backBG->draw();
		middleBG->draw();
	}
}