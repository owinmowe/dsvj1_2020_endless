#ifndef SCENES_MANAGER_H
#define SCENES_MANAGER_H
#include "scenes_base.h"
#include "main_menu.h"
#include "credits.h"
#include "options.h"
#include "in_game.h"

namespace sceneConfig
{

	enum class Scene { MAIN_MENU, OPTIONS, CHANGE_KEYS, CREDITS, IN_GAME, HIGH_SCORE };
	extern SceneBase* currentScene_Ptr;
	extern bool playing;
	extern bool pause;

	void ChangeSceneTo(sceneConfig::Scene nextScene);

}

#endif SCENES_MANAGER_H
