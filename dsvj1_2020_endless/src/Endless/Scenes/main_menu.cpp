#include "main_menu.h"

namespace main_menu
{

	bool startingGame = false;

	MainMenu::MainMenu()
	{
		init();
	}
	void MainMenu::init()
	{
		startingGame = false;
		textures::files::uiTextureFiles[static_cast<int>(textures::files::UI_TEXTURES_ID::GENERAL_BG)].texture.width = screenConfig::SCREEN_WIDTH;
		textures::files::uiTextureFiles[static_cast<int>(textures::files::UI_TEXTURES_ID::GENERAL_BG)].texture.height = screenConfig::SCREEN_HEIGHT;
		buttonSize = { GUI_WIDTH, GUI_HEIGHT };
		setGUITexturesSize();
		audio::menuAudioStart();
		GUI_Ptr[0] = new PlayButton;
		GUI_Ptr[1] = new OptionsButton;
		GUI_Ptr[2] = new CreditsButton;
		GUI_Ptr[3] = new ExitButton;
		GUI_Ptr[0]->selected = true;
	}
	void MainMenu::deInit()
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUI_Ptr[i];
		}
	}
	void MainMenu::update()
	{
		audio::menuAudioUpdate();
		if(startingGame)
		{
			if (input::currentInput.enter)
			{
				sceneConfig::ChangeSceneTo(sceneConfig::Scene::IN_GAME);
				return;
			}
		}
		upDownGUILogic(GUI_SIZE, GUI_Ptr);
	}
	void MainMenu::draw()
	{	
		DrawTexture(textures::files::uiTextureFiles[static_cast<int>(textures::files::UI_TEXTURES_ID::GENERAL_BG)].texture, 0, 0, WHITE);
		if(startingGame)
		{
			ui::drawTextWithSecondFont("Hermes is looking for a successor. Those in search of his favor run", .5f, .5f, 50, BLACK);
			ui::drawTextWithSecondFont("in the sacred mountains in search of sacred items that will grant it.", .5f, .5f + .1f, 50, BLACK);
			ui::drawTextWithSecondFont("You are one of them.", .5f, .5f + .2f, 50, BLACK);
		}
		else
		{
			ui::drawTextWithSecondFont("HERMES CHALLENGE", .5f, .2f, 120, BLACK);
			drawUpDownGUI(GUI_Ptr, GUI_SIZE, GUI_STARTING_POSITION, GUI_SEPARATION);
		}
	}

	std::string PlayButton::getText()
	{
		return derivedText;
	}
	void PlayButton::action()
	{
		startingGame = true;
	}
	std::string OptionsButton::getText()
	{
		return derivedText;
	}
	void OptionsButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::OPTIONS);
	}
	std::string CreditsButton::getText()
	{
		return derivedText;
	}
	void CreditsButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::CREDITS);
	}
	std::string ExitButton::getText()
	{
		return derivedText;
	}
	void ExitButton::action()
	{
		sceneConfig::playing = false;
	}
}