#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Endless/Audio/audio.h"
#include "Endless/Draw/textures.h"
#include "Endless/Ui/ui.h"

namespace main_menu
{

	using namespace ui;

	const int GUI_SIZE = 4;
	const float GUI_STARTING_POSITION = 1 / 3.f;
	const float GUI_SEPARATION = 1 / 6.f;
	const float GUI_WIDTH = 3 / 18.f;
	const float GUI_HEIGHT = 3 / 20.f;

	class MainMenu : public SceneBase
	{

	private:
		void init() override;
		GUIcomponent* GUI_Ptr[GUI_SIZE];

	public:
		MainMenu();
		void deInit() override;
		void update() override;
		void draw() override;

	};

	class PlayButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Play";
	};

	class OptionsButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Options";
	};

	class CreditsButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Credits";
	};

	class ExitButton : public Button
	{
	public:
		bool selected = false;
		std::string getText() override;
		void action() override;

	private:
		std::string derivedText = "Exit";
	};

}

#endif
