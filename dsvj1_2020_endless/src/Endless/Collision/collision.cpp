#include "Collision.h"

namespace collision
{
	using namespace game_objects;
	bool playerCollided(Player* player, DashObject* dashObjects[], JumpObject* jumpObjects[], EvadeObject* evadeObjects[], const int MAX_ENEMIES_FOR_TYPES)
	{
		for (int i = 0; i < MAX_ENEMIES_FOR_TYPES; i++)
		{
			if(dashObjects[i]->isActive() && player->getLanePosition() == dashObjects[i]->getLanePosition() && CheckCollisionRecs(player->getCollider(), dashObjects[i]->getCollider()))
			{
				if(player->getCurrentAnimationState() == ANIMATION_STATE::DASHING)
				{
					dashObjects[i]->deactivate();
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ROCK_BREAK);
					return false;
				}
				else
				{
					return true;
				}
			}
			if (jumpObjects[i]->isActive() && player->getLanePosition() == jumpObjects[i]->getLanePosition() && CheckCollisionRecs(player->getCollider(), jumpObjects[i]->getCollider()))
			{
				return true;
			}
			if (evadeObjects[i]->isActive() && player->getLanePosition() == evadeObjects[i]->getLanePosition() && CheckCollisionRecs(player->getCollider(), evadeObjects[i]->getCollider()))
			{
				return true;
			}
		}
		return false;
	}

	bool playerItemGot(Player* player, int& currentScore, ExtraPointsItem* extraPointsItems[], const int MAX_ENTITIES_FOR_TYPES)
	{
		for (int i = 0; i < MAX_ENTITIES_FOR_TYPES; i++)
		{
			if(extraPointsItems[i]->isActive() && player->getLanePosition() == extraPointsItems[i]->getLanePosition() && CheckCollisionRecs(player->getCollider(), extraPointsItems[i]->getCollider()))
			{
				currentScore += extraPointsItems[i]->getPointsPerPickUp();
				extraPointsItems[i]->deactivate();
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ITEM_GET);
				return true;
			}
		}
		return false;
	}

}