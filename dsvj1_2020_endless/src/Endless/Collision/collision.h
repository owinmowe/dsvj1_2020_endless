#ifndef COLLISION_H
#define COLLISION_H

#include "Endless/GameObject/player.h"
#include "Endless/GameObject/Enemies/dash_object.h"
#include "Endless/GameObject/Enemies/jump_object.h"
#include "Endless/GameObject/Enemies/evade_object.h"
#include "Endless/GameObject/Items/extraPoints.h"

namespace collision
{
	using namespace game_objects;
	bool playerCollided(Player* player, DashObject* dashObjects[], JumpObject* jumpObjects[], EvadeObject* evadeObjects[], const int MAX_ENTITIES_FOR_TYPES);
	bool playerItemGot(Player* player, int& currentScore, ExtraPointsItem* extraPointsItems[], const int MAX_ENTITIES_FOR_TYPES);
}

#endif
