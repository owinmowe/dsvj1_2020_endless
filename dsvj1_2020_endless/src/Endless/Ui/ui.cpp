#include "ui.h"
#include "Endless/Scenes/scene_manager.h"
#include "Endless/ScreenConfig/screen_config.h"
#include "Endless/Input/input.h"
#include "Endless/EventSystem/event_system.h"
#include "Endless/Draw/textures.h"

namespace ui
{

	using namespace textures::files;

	const float BUTTON_BASE_WIDTH = 1 / 4.f;
	const float BUTTON_BASE_HEIGHT = 1 / 10.f;
	const float SLIDER_BASE_WIDTH = 1 / 4.f;
	const float SLIDER_BASE_HEIGHT = 1 / 40.f;

	const Vector2 SLIDER_HANDLE_SIZE = { 0.030f ,  0.038f };
	const int SLIDER_MAX_HANDLE_POSITIONS = 10;

	const int maxSizeTextButton = 25;

	Vector2 buttonSize;
	Vector2 SliderSize;

	void GUIcomponent::update()
	{
		return;
	}
	void GUIcomponent::drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i)
	{
		return;
	}
	void GUIcomponent::drawComponent(float posX, float posY)
	{
		return;
	}

	std::string Button::getText()
	{
		return text;
	}
	void Button::drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i)
	{
		Color buttonColor;
		selected ? buttonColor = GUI_SELECTED_COLOR : buttonColor = GUI_NOT_SELECTED_COLOR;
		Vector2 newPos = { screenConfig::SCREEN_WIDTH / 2 - screenConfig::screenModifier(buttonSize).x / 2, screenConfig::SCREEN_HEIGHT * GUI_STARTING_POSITION + screenConfig::SCREEN_HEIGHT * i * GUI_SEPARATION };
		DrawTexture(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::BUTTON)].texture, static_cast<int>(newPos.x), static_cast<int>(newPos.y), buttonColor);

		char auxText[20];
		strcpy_s(auxText, getText().c_str());
		newPos.x += screenConfig::screenModifier(buttonSize).x / 2 - (MeasureTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, auxText, 50, 1)).x / 2;
		newPos.y += screenConfig::screenModifier(buttonSize).y / 2 - (MeasureTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, auxText, 50, 1)).y / 2;
		DrawTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, auxText, newPos, 50, 1, BLACK);

	}
	void Button::drawComponent(float posX, float posY)
	{
		Color buttonColor;
		selected ? buttonColor = GUI_SELECTED_COLOR : buttonColor = GUI_NOT_SELECTED_COLOR;
		Vector2 newPos = { posX * screenConfig::SCREEN_WIDTH - screenConfig::screenModifier(buttonSize).x / 2, screenConfig::SCREEN_HEIGHT * posY - screenConfig::screenModifier(buttonSize).y / 2 };
		DrawTexture(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::BUTTON)].texture, static_cast<int>(newPos.x), static_cast<int>(newPos.y), buttonColor);

		char auxText[20];
		strcpy_s(auxText, getText().c_str());
		newPos.x += screenConfig::screenModifier(buttonSize).x / 2;
		newPos.y += screenConfig::screenModifier(buttonSize).y / 2;
		DrawTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, auxText, newPos, TEXT_SIZE_SCALE, 1, BLACK);
	}
	void Button::action()
	{
		return;
	}
	void Button::update()
	{
		if (input::currentInput.enter)
		{
			action();
		}
	}

	void Slider::leftAction()
	{
		return;
	}
	void Slider::rightAction()
	{
		return;
	}
	void Slider::update()
	{
		if (input::currentInput.dash && handlePosition < SLIDER_MAX_HANDLE_POSITIONS)
		{
			handlePosition++;
			rightAction();
		}
		else if (input::currentInput.pressed_Left && handlePosition > 0)
		{
			handlePosition--;
			leftAction();
		}
	}
	void Slider::drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i)
	{
		Color sliderColor;
		selected ? sliderColor = GUI_SELECTED_COLOR : sliderColor = GUI_NOT_SELECTED_COLOR;
		Vector2 newPos = { (static_cast<float>(screenConfig::SCREEN_WIDTH / 2 - screenConfig::screenModifier(SliderSize).x / 2)), static_cast<float>(screenConfig::SCREEN_HEIGHT * GUI_STARTING_POSITION + screenConfig::SCREEN_HEIGHT * i * GUI_SEPARATION + screenConfig::screenModifier(SliderSize).y / 2) };
		Vector2 handlePos = newPos;
		DrawTexture(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERBAR)].texture, static_cast<int>(newPos.x), static_cast<int>(newPos.y), WHITE);

		char auxText[20];
		strcpy_s(auxText, getText().c_str());
		newPos.x += screenConfig::screenModifier(SliderSize).x / 2 - MeasureTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, auxText, TEXT_SIZE_SCALE, 1).x / 2;
		newPos.y += screenConfig::screenModifier(SliderSize).y / 2 - MeasureTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, auxText, TEXT_SIZE_SCALE, 1).y * 2;
		DrawTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, auxText, newPos, TEXT_SIZE_SCALE, 1, BLACK);

		Vector2 offset{ 0, 0.005f };
		handlePos.x += (handlePosition * screenConfig::screenModifier(SliderSize).x / SLIDER_MAX_HANDLE_POSITIONS) - uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERHANDLE)].texture.width / 2;
		handlePos.y -= screenConfig::screenModifier(offset).y;
		DrawTexture(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERHANDLE)].texture, static_cast<int>(handlePos.x), static_cast<int>(handlePos.y), sliderColor);

	}
	std::string Slider::getText()
	{
		return text;
	}


	void upDownGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			if (GUI_Ptr[i]->selected)
			{
				GUI_Ptr[i]->update();
				break;
			}
		}

		if (input::currentInput.pressed_Down)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i + 1 < GUI_SIZE)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i + 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
			}
		}
		else if (input::currentInput.pressed_Up)
		{
			for (int i = GUI_SIZE - 1; i >= 0; i--)
			{
				if (GUI_Ptr[i]->selected && i > 0)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i - 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && GUI_SIZE - 1 != 0)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[GUI_SIZE - 1]->selected = true;
					break;
				}
			}
		}
	}

	void leftRightGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			if (GUI_Ptr[i]->selected)
			{
				GUI_Ptr[i]->update();
				break;
			}
		}

		if (input::currentInput.dash)
		{
			for (int i = 0; i < GUI_SIZE; i++)
			{
				if (GUI_Ptr[i]->selected && i + 1 < GUI_SIZE)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i + 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && i != 0)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[0]->selected = true;
					break;
				}
			}
		}
		else if (input::currentInput.pressed_Left)
		{
			for (int i = GUI_SIZE - 1; i >= 0; i--)
			{
				if (GUI_Ptr[i]->selected && i > 0)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[i - 1]->selected = true;
					break;
				}
				else if (GUI_Ptr[i]->selected && GUI_SIZE - 1 != 0)
				{
					GUI_Ptr[i]->selected = false;
					GUI_Ptr[GUI_SIZE - 1]->selected = true;
					break;
				}
			}
		}
	}

	void drawUpDownGUI(GUIcomponent* GUI_Ptr[], const int GUI_SIZE, const float GUI_STARTING_POSITION, const float GUI_SEPARATION)
	{

		for (int i = 0; i < GUI_SIZE; i++)
		{
			GUI_Ptr[i]->drawComponent(GUI_STARTING_POSITION, GUI_SEPARATION, i);
		}

	}

	void setGUITexturesSize()
	{
		uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::BUTTON)].texture.width = static_cast<int>(screenConfig::screenModifier(buttonSize).x);
		uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::BUTTON)].texture.height = static_cast<int>(screenConfig::screenModifier(buttonSize).y);
		uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERBAR)].texture.width = static_cast<int>(screenConfig::screenModifier(SliderSize).x);
		uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERBAR)].texture.height = static_cast<int>(screenConfig::screenModifier(SliderSize).y);
		uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERHANDLE)].texture.width = static_cast<int>(screenConfig::screenModifier(SLIDER_HANDLE_SIZE).x);
		uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERHANDLE)].texture.height = static_cast<int>(screenConfig::screenModifier(SLIDER_HANDLE_SIZE).y);
	}

	void drawTextWithSecondFont(const char* text, float posX, float posY, float fontSize, Color color)
	{
		float auxPosX = screenConfig::screenModifier({ posX, posY }).x - MeasureTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, text, fontSize, 1).x / 2;
		float auxPosY = screenConfig::screenModifier({ posX, posY }).y - MeasureTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, text, fontSize, 1).y / 2;
		DrawTextEx(fontFiles[static_cast<int>(FONTS_ID::GREEK)].font, text, { auxPosX, auxPosY }, fontSize, 1, color);
	}

}