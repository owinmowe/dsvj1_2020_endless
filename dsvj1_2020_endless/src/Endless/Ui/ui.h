#ifndef UI_H
#define UI_H

#include "raylib.h"
#include <string>

namespace ui
{

	extern Vector2 buttonSize;
	extern Vector2 SliderSize;

	class GUIcomponent
	{
	public:
		bool selected = false;
		virtual void update();
		virtual void drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i);
		virtual void drawComponent(float posX, float posY);

	private:
	};
	void drawUpDownGUI(GUIcomponent* GUI_Ptr[], int GUI_SIZE, const float GUI_STARTING_POSITION, const float GUI_SEPARATION);
	void upDownGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	void leftRightGUILogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	void setGUITexturesSize();
	const float TEXT_SIZE_SCALE = 50;
	const Color GUI_SELECTED_COLOR = RED;
	const Color GUI_NOT_SELECTED_COLOR = WHITE;

	class Button : public GUIcomponent
	{
	public:
		void drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i) override;
		void drawComponent(float posX, float posY) override;
		virtual void action();
		virtual std::string getText();
	private:
		void update() override;
		std::string text;
	};

	class Slider : public GUIcomponent
	{
	public:
		void drawComponent(const float GUI_STARTING_POSITION, const float GUI_SEPARATION, int i) override;
		virtual void leftAction();
		virtual void rightAction();
		virtual std::string getText();
		int handlePosition = 0;
	private:
		void update() override;
		std::string text;
	};

	void drawTextWithSecondFont(const char* text, float posX, float posY, float fontSize, Color color);

}

#endif UI_H
