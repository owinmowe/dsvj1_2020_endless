#include "screen_config.h"

namespace screenConfig
{
    
    int currentFrame = 0;

    //Modifies size/pos to adjust to screen size being 0 null and 1 fullscreen 
    Vector2 screenModifier(Vector2 size)
    {
        Vector2 newVector2;
        newVector2.x = size.x * SCREEN_WIDTH;
        newVector2.y = size.y * SCREEN_HEIGHT;
        return newVector2;
    }

    float textScreenModifier(float size)
    {
        return static_cast<float>(size * screenConfig::SCREEN_WIDTH);
    }
    
}