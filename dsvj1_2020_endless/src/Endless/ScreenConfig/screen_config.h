#ifndef SCREEN_CONFIG_H
#define SCREEN_CONFIG_H

#include "raylib.h"

namespace screenConfig
{
    const int SCREEN_WIDTH = 1920;
    const int SCREEN_HEIGHT = 1080;
    const int FRAMES_PER_SECOND = 60;
    extern int currentFrame;
    Vector2 screenModifier(Vector2 size);
    float textScreenModifier(float size);
   
}

#endif
