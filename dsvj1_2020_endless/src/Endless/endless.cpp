#include "Endless.h"

Endless::Endless()
{
    InitWindow(screenConfig::SCREEN_WIDTH, screenConfig::SCREEN_HEIGHT, "");
    InitAudioDevice();
    audio::loadMusicInMemory();
    audio::loadSoundsInMemory();
    audio::setAllSoundVolume();
    audio::setAllMusicVolume();
    textures::LoadUiTextures();
    event_system::externalEventSystem = new event_system::EventSystem;
    SetTargetFPS(screenConfig::FRAMES_PER_SECOND);
    SetExitKey(0);
    HideCursor();
    sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
}
Endless::~Endless()
{
    audio::unloadMusicFromMemory();
    audio::unloadSoundsFromMemory();
    textures::UnloadUiTextures();
    if (IsWindowFullscreen()) { ToggleFullscreen(); } // Go to windows mode before closing to avoid crashing opengl. 
    CloseWindow();
}
void Endless::input()
{
    input::updateInputGame();
}
void Endless::update()
{
    updateGame();
}
void Endless::draw()
{
    drawGame();
}
void Endless::play()
{
    while (!WindowShouldClose() && sceneConfig::playing)
    {
        input();
        update();
        draw();
        event_system::externalEventSystem->ResetEvents();
    }
}