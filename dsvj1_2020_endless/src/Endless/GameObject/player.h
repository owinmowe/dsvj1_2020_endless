#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

#include "game_object.h"

namespace game_objects
{

	enum class ANIMATION_STATE { GROUNDED, JUMPING, DASHING };

	class Player : public GameObject
	{
	public:
		Player(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos);
		void moveUp();
		void moveDown();
		void jump();
		void dash();
		void update() override;

		void setPlayerCustomCollider();
		ANIMATION_STATE getCurrentAnimationState();

	private:
		float playerWidth = .4f;
		float playerHeight = .4f;
		float playerPosLaneUp = .375f;
		float playerPosLaneMiddle = .5f;
		float playerPosLaneDown = .625f;
		Vector2 velocity = { 0, 0 };
		Vector2 playerGoundPos = { 0, 0 };
		float playerJumpStrenght = 1400;
		int playerRunAnimationSpeed = 8;
		int playerDashAnimationSpeed = 4;
		void nextRectAnimationRecX();
		void resetAnimationRecX();
		void nextRectAnimationRecY();
		void resetAnimationRecY();
		const int RUN_ANIMATION_FRAMES = 4;
		const int JUMP_MAX_FRAMES = 100;
		const int DASH_ANIMATION_FRAMES = 4;
		const int DASH_MAX_FRAMES = 90;
		ANIMATION_STATE currentAnimationState = ANIMATION_STATE::GROUNDED;
		int currentMoveAnimationFrame = 1;
		int currentJumpAnimationFrame = 1;
		int currentDashAnimationFrame = 1;
	};
}
#endif
