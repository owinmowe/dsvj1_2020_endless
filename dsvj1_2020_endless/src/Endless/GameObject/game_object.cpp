#include "game_object.h"

namespace game_objects
{
	GameObject::GameObject()
	{
		texture = nullptr;
		position = { 0, 0 };
		rectangle = { 0, 0, 0, 0 };
		collider = { 0, 0, 0, 0 };
		setLanePosition(LANE_POSITION::MIDDLE);
		posLaneUp = .375f;
		posLaneMiddle = .5f;
		posLaneDown = .625f;
	}
	Texture2D* GameObject::getTexture()
	{
		return texture;
	}
	void GameObject::setTexture2D(Texture2D* tex)
	{
		texture = tex;
	}
	Rectangle GameObject::getRectangle()
	{
		return rectangle;
	}
	void GameObject::setRectangle(Rectangle rec)
	{
		rectangle = rec;
	}
	void GameObject::setBasicGameObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos)
	{
		texture = tex;
		rectangle = rec;
		currentLanePosition = lanePos;
	}
	void GameObject::draw()
	{
		DrawTextureRec(*getTexture(), getRectangle(), position, global_ilumination::getCurrentIluminationColor());
	}
	void GameObject::update()
	{
		return;
	}
	void GameObject::setLanePosition(LANE_POSITION lanePos)
	{
		currentLanePosition = lanePos;
		switch (currentLanePosition)
		{
		case game_objects::LANE_POSITION::UP:
			position.x = screenConfig::screenModifier({ 0, posLaneUp }).x;
			position.y = screenConfig::screenModifier({ 0, posLaneUp }).y;
			break;
		case game_objects::LANE_POSITION::MIDDLE:
			position.x = screenConfig::screenModifier({ 0, posLaneMiddle }).x;
			position.y = screenConfig::screenModifier({ 0, posLaneMiddle }).y;
			break;
		case game_objects::LANE_POSITION::DOWN:
			position.x = screenConfig::screenModifier({ 0, posLaneDown }).x;
			position.y = screenConfig::screenModifier({ 0, posLaneDown }).y;
			break;
		default:
			break;
		}
		collider = rectangle;
	}
	LANE_POSITION GameObject::getLanePosition()
	{
		return currentLanePosition;
	}
	Vector2 GameObject::getPosition()
	{
		return position;
	}
	Rectangle GameObject::getCollider()
	{
		return collider;
	}
}