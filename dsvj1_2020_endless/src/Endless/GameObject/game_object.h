#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "raylib.h"

#include "Endless/Draw/post_processing.h"
#include "Endless/ScreenConfig/screen_config.h"
#include "Endless/EventSystem/event_system.h"

namespace game_objects
{

	enum class LANE_POSITION{UP, DOWN, MIDDLE, SIZE};

	class GameObject
	{
	public:
		GameObject();
		Texture2D* getTexture();
		void setTexture2D(Texture2D* tex);
		Rectangle getRectangle();
		void setRectangle(Rectangle rec);
		void setBasicGameObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos);
		void setLanePosition(LANE_POSITION lanePos);
		LANE_POSITION getLanePosition();
		Vector2 getPosition();
		Rectangle getCollider();
		virtual void update();
		void draw();
	protected:
		float posLaneUp;
		float posLaneDown;
		float posLaneMiddle;
		Vector2 position = { 0,0 };
		Rectangle collider = { 0, 0, 0, 0 };
	private:
		Texture2D* texture = nullptr;
		Rectangle rectangle = {0,0,0,0};
		LANE_POSITION currentLanePosition = LANE_POSITION::MIDDLE;
	};
}

#endif
