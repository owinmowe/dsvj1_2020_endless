#ifndef DASH_OBJECT_H
#define DASH_OBJECT_H

#include "Endless/GameObject/game_object.h"

namespace game_objects
{
	class DashObject : public GameObject
	{
	public:
		DashObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos);
		void update() override;
		void activate(float spd);
		void deactivate();
		bool isActive();
		void resetPositionX();
	private:
		void RandomLanePosition();
		float dashObjectWidth = .1f;
		float dashObjectHeight = .175f;
		float dashObjectPosLaneUp = .400f;
		float dashObjectPosLaneMiddle = .525f;
		float dashObjectPosLaneDown = .65f;
		bool active;
		float speed;
	};
}

#endif
