#include "jump_object.h"

#include "Endless/GameObject/game_object.h"

namespace game_objects
{
	JumpObject::JumpObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos)
	{
		active = false;
		speed = 0;
		tex->width = static_cast<int>(screenConfig::screenModifier({ jumpObjectWidth, jumpObjectHeight }).x);
		tex->height = static_cast<int>(screenConfig::screenModifier({ jumpObjectWidth, jumpObjectHeight }).y);
		setTexture2D(tex);
		rec.width = screenConfig::screenModifier({ jumpObjectWidth, jumpObjectHeight }).x;
		rec.height = screenConfig::screenModifier({ jumpObjectWidth, jumpObjectHeight }).y;
		setRectangle(rec);
		posLaneDown = jumpObjectPosLaneDown;
		posLaneMiddle = jumpObjectPosLaneMiddle;
		posLaneUp = jumpObjectPosLaneUp;
		setLanePosition(lanePos);
		position.x = screenConfig::SCREEN_WIDTH;
	}
	void JumpObject::RandomLanePosition()
	{
		LANE_POSITION randomLane = static_cast<LANE_POSITION>(GetRandomValue(0, static_cast<int>(LANE_POSITION::SIZE) - 1));
		setLanePosition(randomLane);
	}
	void JumpObject::update()
	{
		position.x -= static_cast<int>(speed * GetFrameTime() * screenConfig::SCREEN_WIDTH);
		setCustomJumpObjectCollider();
		if (position.x + getTexture()->width < 0)
		{
			active = false;
		}
	}
	void JumpObject::setCustomJumpObjectCollider()
	{
		collider.x = position.x + getRectangle().width / 6;
		collider.width = getRectangle().width - getRectangle().width / 2;
		collider.y = position.y + getRectangle().height / 10;
		collider.height = getRectangle().height - getRectangle().height / 5;
	}
	void JumpObject::activate(float spd)
	{
		RandomLanePosition();
		position.x = screenConfig::SCREEN_WIDTH;
		active = true;
		speed = spd;
	}
	bool JumpObject::isActive()
	{
		return active;
	}
	void JumpObject::resetPositionX()
	{
		position.x = screenConfig::SCREEN_WIDTH;
	}
}