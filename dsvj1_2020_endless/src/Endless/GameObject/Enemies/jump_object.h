#ifndef JUMP_OBJECT_H
#define JUMP_OBJECT_H

#include "Endless/GameObject/game_object.h"

namespace game_objects
{
	class JumpObject : public GameObject
	{
	public:
		JumpObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos);
		void update() override;
		void setCustomJumpObjectCollider();
		void activate(float spd);
		bool isActive();
		void resetPositionX();
	private:
		void RandomLanePosition();
		float jumpObjectWidth = .075f;
		float jumpObjectHeight = .125f;
		float jumpObjectPosLaneUp = .475f;
		float jumpObjectPosLaneMiddle = .600f;
		float jumpObjectPosLaneDown = .725f;
		bool active;
		float speed;
	};
}

#endif