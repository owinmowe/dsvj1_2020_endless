#include "dash_object.h"

#include "Endless/GameObject/game_object.h"

namespace game_objects
{
	DashObject::DashObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos)
	{
		active = false;
		speed = 0;
		tex->width = static_cast<int>(screenConfig::screenModifier({ dashObjectWidth, dashObjectHeight }).x);
		tex->height = static_cast<int>(screenConfig::screenModifier({ dashObjectWidth, dashObjectHeight }).y);
		setTexture2D(tex);
		rec.width = screenConfig::screenModifier({ dashObjectWidth, dashObjectHeight }).x;
		rec.height = screenConfig::screenModifier({ dashObjectWidth, dashObjectHeight }).y;
		setRectangle(rec);
		posLaneDown = dashObjectPosLaneDown;
		posLaneMiddle = dashObjectPosLaneMiddle;
		posLaneUp = dashObjectPosLaneUp;
		setLanePosition(lanePos);
		position.x = screenConfig::SCREEN_WIDTH;
	}
	void DashObject::RandomLanePosition()
	{
		LANE_POSITION randomLane = static_cast<LANE_POSITION>(GetRandomValue(0, static_cast<int>(LANE_POSITION::SIZE) - 1));
		setLanePosition(randomLane);
	}
	void DashObject::update()
	{
		position.x -= static_cast<int>(speed * GetFrameTime() * screenConfig::SCREEN_WIDTH);
		collider.x = position.x;
		collider.y = position.y;
		if(position.x + getTexture()->width < 0)
		{
			active = false;
		}
	}
	void DashObject::activate(float spd)
	{
		RandomLanePosition();
		position.x = screenConfig::SCREEN_WIDTH;
		active = true;
		speed = spd;
	}
	void DashObject::deactivate()
	{
		active = false;
	}
	bool DashObject::isActive()
	{
		return active;
	}
	void DashObject::resetPositionX()
	{
		position.x = screenConfig::SCREEN_WIDTH;
	}
}
