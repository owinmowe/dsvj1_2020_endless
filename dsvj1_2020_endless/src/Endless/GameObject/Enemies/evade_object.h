#ifndef EVADE_OBJECT_H
#define EVADE_OBJECT_H

#include "Endless/GameObject/game_object.h"

namespace game_objects
{
	class EvadeObject : public GameObject
	{
	public:
		EvadeObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos);
		void update() override;
		void activate(float spd);
		bool isActive();
		void resetPositionX();
	private:
		void RandomLanePosition();
		float evadeObjectWidth = .1f;
		float evadeObjectHeight = .225f;
		float evadeObjectPosLaneUp = .350f;
		float evadeObjectPosLaneMiddle = .475f;
		float evadeObjectPosLaneDown = .6f;
		bool active;
		float speed;
	};
}

#endif
