#include "evade_object.h"

#include "Endless/GameObject/game_object.h"

namespace game_objects
{
	EvadeObject::EvadeObject(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos)
	{
		active = false;
		speed = 0;
		tex->width = static_cast<int>(screenConfig::screenModifier({ evadeObjectWidth, evadeObjectHeight }).x);
		tex->height = static_cast<int>(screenConfig::screenModifier({ evadeObjectWidth, evadeObjectHeight }).y);
		setTexture2D(tex);
		rec.width = screenConfig::screenModifier({ evadeObjectWidth, evadeObjectHeight }).x;
		rec.height = screenConfig::screenModifier({ evadeObjectWidth, evadeObjectHeight }).y;
		setRectangle(rec);
		posLaneDown = evadeObjectPosLaneDown;
		posLaneMiddle = evadeObjectPosLaneMiddle;
		posLaneUp = evadeObjectPosLaneUp;
		setLanePosition(lanePos);
		position.x = screenConfig::SCREEN_WIDTH;
	}
	void EvadeObject::RandomLanePosition()
	{
		LANE_POSITION randomLane = static_cast<LANE_POSITION>(GetRandomValue(0, static_cast<int>(LANE_POSITION::SIZE) - 1));
		setLanePosition(randomLane);
	}
	void EvadeObject::update()
	{
		position.x -= static_cast<int>(speed * GetFrameTime() * screenConfig::SCREEN_WIDTH);
		collider.x = position.x;
		collider.y = position.y;
		if (position.x + getTexture()->width < 0)
		{
			active = false;
		}
	}
	void EvadeObject::activate(float spd)
	{
		RandomLanePosition();
		position.x = screenConfig::SCREEN_WIDTH;
		active = true;
		speed = spd;
	}
	bool EvadeObject::isActive()
	{
		return active;
	}
	void EvadeObject::resetPositionX()
	{
		position.x = screenConfig::SCREEN_WIDTH;
	}
}