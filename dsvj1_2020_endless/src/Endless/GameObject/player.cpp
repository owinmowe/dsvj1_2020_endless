#include "player.h"

namespace game_objects
{
	Player::Player(Texture2D* tex, Rectangle rec, LANE_POSITION lanePos)
	{
		tex->width = static_cast<int>(screenConfig::screenModifier({ playerWidth, playerHeight }).x);
		tex->height = static_cast<int>(screenConfig::screenModifier({ playerWidth, playerHeight }).y);
		setTexture2D(tex);
		rec.width = screenConfig::screenModifier({ playerWidth, playerHeight }).x / RUN_ANIMATION_FRAMES;
		rec.height = (screenConfig::screenModifier({ playerWidth, playerHeight }).y / 2);
		setRectangle(rec);
		posLaneDown = playerPosLaneDown;
		posLaneMiddle = playerPosLaneMiddle;
		posLaneUp = playerPosLaneUp;
		setLanePosition(lanePos);
		velocity = { 0, 0 };
		playerGoundPos = { 0, 0 };
	}
	void Player::update()
	{

		switch (currentAnimationState)
		{
		case game_objects::ANIMATION_STATE::GROUNDED:
			if (screenConfig::currentFrame % playerRunAnimationSpeed == 0)
			{
				currentMoveAnimationFrame++;
				nextRectAnimationRecX();
				if (currentMoveAnimationFrame > RUN_ANIMATION_FRAMES)
				{
					currentMoveAnimationFrame = 1;
					resetAnimationRecX();
				}
			}
			break;
		case game_objects::ANIMATION_STATE::JUMPING:
			resetAnimationRecX();
			currentJumpAnimationFrame++;
			position.y -= velocity.y;
			velocity.y--;
			if (position.y > playerGoundPos.y)
			{
				position = playerGoundPos;
				currentJumpAnimationFrame = 1;
				currentMoveAnimationFrame = 1;
				currentAnimationState = ANIMATION_STATE::GROUNDED;
			}
			break;
		case game_objects::ANIMATION_STATE::DASHING:
			currentDashAnimationFrame++;
			if (screenConfig::currentFrame % playerDashAnimationSpeed == 0)
			{
				nextRectAnimationRecX();
			}
			if (currentDashAnimationFrame > DASH_MAX_FRAMES)
			{
				currentDashAnimationFrame = 1;
				currentMoveAnimationFrame = 1;
				resetAnimationRecY();
				resetAnimationRecX();
				currentAnimationState = ANIMATION_STATE::GROUNDED;
			}
			break;
		default:
			break;
		}
		setPlayerCustomCollider();
	}	
	
	void Player::setPlayerCustomCollider()
	{
		collider.x = position.x + getRectangle().width / 3;
		collider.width = getRectangle().width - getRectangle().width * 9 / 12;
		collider.y = position.y + getRectangle().height / 10;
		collider.height = getRectangle().height - getRectangle().height / 5;
	}
	void Player::moveUp()
	{
		if (currentAnimationState != ANIMATION_STATE::GROUNDED) { return; }
		if(getLanePosition() == LANE_POSITION::DOWN)
		{
			setLanePosition(LANE_POSITION::MIDDLE);
			event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE);
		}
		else if(getLanePosition() == LANE_POSITION::MIDDLE)
		{
			setLanePosition(LANE_POSITION::UP);
			event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE);
		}
	}
	void Player::moveDown()
	{
		if (currentAnimationState != ANIMATION_STATE::GROUNDED) { return; }
		if (getLanePosition() == LANE_POSITION::UP)
		{
			setLanePosition(LANE_POSITION::MIDDLE);
			event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE);
		}
		else if (getLanePosition() == LANE_POSITION::MIDDLE)
		{
			setLanePosition(LANE_POSITION::DOWN);
			event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE);
		}
	}
	void Player::dash()
	{
		if (currentAnimationState != ANIMATION_STATE::GROUNDED) { return; }
		event_system::externalEventSystem->setEvent(event_system::EVENT_ID::DASH);
		nextRectAnimationRecY();
		currentAnimationState = ANIMATION_STATE::DASHING;
	}
	void Player::jump()
	{
		if (currentAnimationState != ANIMATION_STATE::GROUNDED) { return; }
		event_system::externalEventSystem->setEvent(event_system::EVENT_ID::JUMP);
		playerGoundPos = position;
		velocity.y = playerJumpStrenght * GetFrameTime();
		currentAnimationState = ANIMATION_STATE::JUMPING;
	}
	ANIMATION_STATE Player::getCurrentAnimationState()
	{
		return currentAnimationState;
	}
	void Player::nextRectAnimationRecX()
	{
		Rectangle aux = getRectangle();
		aux.x += getTexture()->width / RUN_ANIMATION_FRAMES;
		setRectangle(aux);
	}
	void Player::resetAnimationRecX()
	{
		Rectangle aux = getRectangle();
		aux.x = 0;
		setRectangle(aux);
	}
	void Player::nextRectAnimationRecY()
	{
		Rectangle aux = getRectangle();
		aux.y += getTexture()->height / 2;
		setRectangle(aux);
	}
	void Player::resetAnimationRecY()
	{
		Rectangle aux = getRectangle();
		aux.y = 0;
		setRectangle(aux);
	}
}