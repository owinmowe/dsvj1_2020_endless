#ifndef EXTRA_POINTS_H
#define EXTRA_POINTS_H

#include "Endless/GameObject/game_object.h"

namespace game_objects
{

	enum class EXTRA_POINTS_TYPE{RUN, JUMP, DASH};
	const int POINTS_RUN_ITEM = 200;
	const int POINTS_DASH_ITEM = 300;
	const int POINTS_JUMP_ITEM = 400;

	class ExtraPointsItem : public game_objects::GameObject
	{
	public:
		ExtraPointsItem(Texture2D* texRun, Texture2D* texDash, Texture2D* texJump, Rectangle rec, LANE_POSITION lanePos);
		void update() override;
		void activate(float spd, EXTRA_POINTS_TYPE type);
		void activate(float spd, EXTRA_POINTS_TYPE type, LANE_POSITION lanePos, bool flying);
		void deactivate();
		bool isActive();
		int getPointsPerPickUp();
		void resetPositionX();
	private:
		Texture2D* textureRun;
		Texture2D* textureDash;
		Texture2D* textureJump;
		void RandomLanePosition();
		float extraPointsWidth = .035f;
		float extraPointsHeight = .05f;
		float extraPointsObjectPosLaneUp = .510f;
		float extraPointsPosLaneMiddle = .640f;
		float extraPointsPosLaneDown = .775f;
		int pointsPerPickUp;
		bool active;
		float speed;
	};
}




#endif

