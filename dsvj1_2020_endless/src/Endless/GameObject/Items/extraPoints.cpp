#include "extraPoints.h"

namespace game_objects
{
	ExtraPointsItem::ExtraPointsItem(Texture2D* texRun, Texture2D* texDash, Texture2D* texJump, Rectangle rec, LANE_POSITION lanePos)
	{
		active = false;
		speed = 0;
		pointsPerPickUp = 0;
		texRun->width = static_cast<int>(screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).x);
		texRun->height = static_cast<int>(screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).y);
		textureRun = texRun;
		texDash->width = static_cast<int>(screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).x);
		texDash->height = static_cast<int>(screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).y);
		textureDash = texDash;
		texJump->width = static_cast<int>(screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).x);
		texJump->height = static_cast<int>(screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).y);
		textureJump = texJump;
		rec.width = screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).x;
		rec.height = screenConfig::screenModifier({ extraPointsWidth, extraPointsHeight }).y;
		setRectangle(rec);
		posLaneDown = extraPointsPosLaneDown;
		posLaneMiddle = extraPointsPosLaneMiddle;
		posLaneUp = extraPointsObjectPosLaneUp;
		setLanePosition(lanePos);
		position.x = screenConfig::SCREEN_WIDTH;
	}
	void ExtraPointsItem::RandomLanePosition()
	{
		LANE_POSITION randomLane = static_cast<LANE_POSITION>(GetRandomValue(0, static_cast<int>(LANE_POSITION::SIZE) - 1));
		setLanePosition(randomLane);
	}
	void ExtraPointsItem::update()
	{
		position.x -= static_cast<int>(speed * GetFrameTime() * screenConfig::SCREEN_WIDTH);
		collider.x = position.x;
		collider.y = position.y;
		if (position.x + getTexture()->width < 0)
		{
			active = false;
		}
	}
	void ExtraPointsItem::activate(float spd, EXTRA_POINTS_TYPE type)
	{
		RandomLanePosition();
		switch (type)
		{
		case game_objects::EXTRA_POINTS_TYPE::RUN:
			pointsPerPickUp = POINTS_RUN_ITEM;
			setTexture2D(textureRun);
			break;
		case game_objects::EXTRA_POINTS_TYPE::JUMP:
			pointsPerPickUp = POINTS_JUMP_ITEM;
			setTexture2D(textureJump);
			break;
		case game_objects::EXTRA_POINTS_TYPE::DASH:
			pointsPerPickUp = POINTS_DASH_ITEM;
			setTexture2D(textureDash);
			break;
		default:
			break;
		}
		position.x = screenConfig::SCREEN_WIDTH;
		active = true;
		speed = spd;
	}
	void ExtraPointsItem::activate(float spd, EXTRA_POINTS_TYPE type, LANE_POSITION lanePos, bool flying)
	{
		setLanePosition(lanePos);
		switch (type)
		{
		case game_objects::EXTRA_POINTS_TYPE::RUN:
			pointsPerPickUp = POINTS_RUN_ITEM;
			setTexture2D(textureRun);
			break;
		case game_objects::EXTRA_POINTS_TYPE::JUMP:
			pointsPerPickUp = POINTS_JUMP_ITEM;
			setTexture2D(textureJump);
			break;
		case game_objects::EXTRA_POINTS_TYPE::DASH:
			pointsPerPickUp = POINTS_DASH_ITEM;
			setTexture2D(textureDash);
			break;
		default:
			break;
		}
		if(flying)
		{
			position.x = screenConfig::SCREEN_WIDTH + getRectangle().width * 1 / 2;
			position.y -= getRectangle().height * 6;
		}
		else
		{
			position.x = screenConfig::SCREEN_WIDTH + getRectangle().width * 3;
			position.y -= getRectangle().height * 11/12;
		}
		active = true;
		speed = spd;
	}
	void ExtraPointsItem::deactivate()
	{
		active = false;
	}
	bool ExtraPointsItem::isActive()
	{
		return active;
	}
	int ExtraPointsItem::getPointsPerPickUp()
	{
		return pointsPerPickUp;
	}
	void ExtraPointsItem::resetPositionX()
	{
		position.x = screenConfig::SCREEN_WIDTH;
	}
}
