#include "textures.h"
#include "raylib.h"
#include <string>
#include <iostream>

namespace textures
{

	namespace files
	{

		const int MAX_TEXTURE_FILES = 20;

		std::vector<FontFile> fontFiles (MAX_TEXTURE_FILES);
		std::vector<TextureFile> uiTextureFiles (MAX_TEXTURE_FILES);
		std::vector<TextureFile> backgroundTextureFiles (MAX_TEXTURE_FILES);
		std::vector<TextureFile> gameObjectsTextureFiles (MAX_TEXTURE_FILES);

		void setFontsVector()
		{
			std::string fontString = "res/assets/fonts/Greek.ttf";
			fontString.copy(fontFiles[static_cast<int>(FONTS_ID::GREEK)].FILE_PATH, fontString.size() + 1);
		}

		void setUiTexturesVector()
		{
			std::string buttonString = "res/assets/textures/ui/button.png";
			buttonString.copy(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::BUTTON)].FILE_PATH, buttonString.size() + 1);
			std::string sliderBarString = "res/assets/textures/ui/sliderBar.png";
			sliderBarString.copy(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERBAR)].FILE_PATH, sliderBarString.size() + 1);
			std::string sliderHandleString = "res/assets/textures/ui/sliderHandle.png";
			sliderHandleString.copy(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::SLIDERHANDLE)].FILE_PATH, sliderHandleString.size() + 1);
			std::string generalBackground = "res/assets/textures/backgrounds/general.png";
			generalBackground.copy(uiTextureFiles[static_cast<int>(UI_TEXTURES_ID::GENERAL_BG)].FILE_PATH, generalBackground.size() + 1);
		}

		void setBackgroundTexturesVector()
		{
			std::string frontBGString = "res/assets/textures/backgrounds/FrontBG.png";
			frontBGString.copy(backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::FRONT_BG)].FILE_PATH, frontBGString.size() + 1);
			std::string middleBGString = "res/assets/textures/backgrounds/MiddleBG.png";
			middleBGString.copy(backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::MIDDLE_BG)].FILE_PATH, middleBGString.size() + 1);
			std::string backBGString = "res/assets/textures/backgrounds/BackBG.png";
			backBGString.copy(backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::BACK_BG)].FILE_PATH, backBGString.size() + 1);
			std::string dayNightBGString = "res/assets/textures/backgrounds/DayNightBG.png";
			dayNightBGString.copy(backgroundTextureFiles[static_cast<int>(BACKGROUND_TEXTURES_ID::DAY_NIGHT_BG)].FILE_PATH, dayNightBGString.size() + 1);
		}

		void setGameObjectsTexturesVector()
		{
			std::string playerString = "res/assets/textures/gameObjects/Player.png";
			playerString.copy(gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::PLAYER)].FILE_PATH, playerString.size() + 1);
			std::string evadeObjectString = "res/assets/textures/gameObjects/EvadeObject.png";
			evadeObjectString.copy(gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EVADE_OBSTACLE)].FILE_PATH, evadeObjectString.size() + 1);
			std::string jumpObjectString = "res/assets/textures/gameObjects/jumpObject.png";
			jumpObjectString.copy(gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::JUMP_OBSTACLE)].FILE_PATH, jumpObjectString.size() + 1);
			std::string dashObjectString = "res/assets/textures/gameObjects/dashObject.png";
			dashObjectString.copy(gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::DASH_OBSTACLE)].FILE_PATH, dashObjectString.size() + 1);
			std::string extraPointsRunObjectString = "res/assets/textures/gameObjects/extraPointsObjectRun.png";
			extraPointsRunObjectString.copy(gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EXTRA_POINTS_RUN_ITEM)].FILE_PATH, extraPointsRunObjectString.size() + 1);
			std::string extraPointsDashObjectString = "res/assets/textures/gameObjects/extraPointsObjectDash.png";
			extraPointsDashObjectString.copy(gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EXTRA_POINTS_DASH_ITEM)].FILE_PATH, extraPointsDashObjectString.size() + 1);
			std::string extraPointsJumpObjectString = "res/assets/textures/gameObjects/extraPointsObjectJump.png";
			extraPointsJumpObjectString.copy(gameObjectsTextureFiles[static_cast<int>(GAMEOBJECTS_TEXTURES_ID::EXTRA_POINTS_JUMP_ITEM)].FILE_PATH, extraPointsJumpObjectString.size() + 1);

		}

	}

	void LoadUiTextures()
	{
		files::setFontsVector();
		for (int i = 0; i < static_cast<int>(files::fontFiles.size()); i++)
		{
			files::fontFiles[i].font = LoadFontEx(files::fontFiles[i].FILE_PATH, 200, 0, NULL);
		}

		files::setUiTexturesVector();
		for (int i = 0; i < static_cast<int>(files::uiTextureFiles.size()); i++)
		{
			files::uiTextureFiles[i].texture = LoadTexture(files::uiTextureFiles[i].FILE_PATH);
		}		
	}
	void UnloadUiTextures()
	{
		for (int i = 0; i < static_cast<int>(files::fontFiles.size()); i++)
		{
			UnloadFont(files::fontFiles[i].font);
		}

		for (int i = 0; i < static_cast<int>(files::uiTextureFiles.size()); i++)
		{
			UnloadTexture(files::uiTextureFiles[i].texture);
		}
	}

	void LoadGameTextures()
	{
		files::setBackgroundTexturesVector();
		for (int i = 0; i < static_cast<int>(files::backgroundTextureFiles.size()); i++)
		{
			files::backgroundTextureFiles[i].texture = LoadTexture(files::backgroundTextureFiles[i].FILE_PATH);
			std::cout << files::backgroundTextureFiles[i].FILE_PATH;
		}

		files::setGameObjectsTexturesVector();
		for (int i = 0; i < static_cast<int>(files::gameObjectsTextureFiles.size()); i++)
		{
			files::gameObjectsTextureFiles[i].texture = LoadTexture(files::gameObjectsTextureFiles[i].FILE_PATH);
		}
	}
	void UnloadGameTextures()
	{
		for (int i = 0; i < static_cast<int>(files::backgroundTextureFiles.size()); i++)
		{
			UnloadTexture(files::backgroundTextureFiles[i].texture);
		}
		for (int i = 0; i < static_cast<int>(files::gameObjectsTextureFiles.size()); i++)
		{
			UnloadTexture(files::gameObjectsTextureFiles[i].texture);
		}
	}

}