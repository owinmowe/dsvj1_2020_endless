#ifndef POST_PROCESSING_H
#define POST_PROCESSING_H

#include "raylib.h"

namespace global_ilumination
{
	Color getCurrentIluminationColor();
	void updateIlumination();
	void resetIllumination();
}

#endif
