#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

#include <vector>

namespace textures
{

	namespace files
	{

		const int MAX_SIZE_FILEPATH = 100;

		struct FontFile
		{
			Font font;
			char FILE_PATH[MAX_SIZE_FILEPATH];
		};

		struct TextureFile
		{
			Texture2D texture;
			char FILE_PATH[MAX_SIZE_FILEPATH];
		};

		extern std::vector<FontFile> fontFiles;
		enum class FONTS_ID{GREEK};

		extern std::vector<TextureFile> uiTextureFiles;
		enum class UI_TEXTURES_ID{BUTTON, SLIDERBAR, SLIDERHANDLE, GENERAL_BG};

		extern std::vector<TextureFile> backgroundTextureFiles;
		enum class BACKGROUND_TEXTURES_ID {FRONT_BG, MIDDLE_BG, BACK_BG, DAY_NIGHT_BG};

		extern std::vector<TextureFile> gameObjectsTextureFiles;
		enum class GAMEOBJECTS_TEXTURES_ID {PLAYER, JUMP_OBSTACLE, DASH_OBSTACLE, EVADE_OBSTACLE, EXTRA_POINTS_RUN_ITEM, EXTRA_POINTS_DASH_ITEM, EXTRA_POINTS_JUMP_ITEM};

	}

	void LoadUiTextures();
	void UnloadUiTextures();
	void LoadGameTextures();
	void UnloadGameTextures();

}

#endif TEXTURES_H
