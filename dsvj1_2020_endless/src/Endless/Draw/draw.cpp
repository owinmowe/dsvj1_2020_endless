#include "draw.h"

#include "Endless/ScreenConfig/screen_config.h"
#include "Endless/Scenes/scene_manager.h"

void frameCounter();

void drawGame()
{

    frameCounter();
    BeginDrawing();
    ClearBackground(BLACK);
    sceneConfig::currentScene_Ptr->draw();
    EndDrawing();

}

void frameCounter()
{
    screenConfig::currentFrame++;
    if (screenConfig::currentFrame == screenConfig::FRAMES_PER_SECOND)
    {
        screenConfig::currentFrame = 0;
    }
}