#include "post_processing.h"

namespace global_ilumination
{

	enum class DAY_MOMENT { MORNING, DAY, NOON, NIGHT };
	DAY_MOMENT currentDayMoment = DAY_MOMENT::DAY;
	const int MAX_FRAMES_TO_CHANGE_ILUMINATION = 20;
	Color currentIlumination = WHITE;
	int iluminationFrame = 0;
	
	Color getCurrentIluminationColor()
	{
		return currentIlumination;
	}

	void resetIllumination()
	{
		currentDayMoment = DAY_MOMENT::DAY;
		currentIlumination = WHITE;
	}

	void updateIlumination()
	{
		iluminationFrame++;
		if(iluminationFrame == MAX_FRAMES_TO_CHANGE_ILUMINATION)
		{
			bool dayChange = true;
			switch (currentDayMoment)
			{
			case global_ilumination::DAY_MOMENT::MORNING:
				if (currentIlumination.b != WHITE.b)
				{
					currentIlumination.b++;
					dayChange = false;
				}
				if (currentIlumination.r != WHITE.r)
				{
					currentIlumination.r++;
					dayChange = false;
				}
				if (currentIlumination.g != WHITE.g)
				{
					currentIlumination.g++;
					dayChange = false;
				}
				if (dayChange)
				{
					currentDayMoment = DAY_MOMENT::DAY;
				}
				break;
			case global_ilumination::DAY_MOMENT::DAY:
				if (currentIlumination.b != RED.b)
				{
					currentIlumination.b--;
					dayChange = false;
				}
				if (currentIlumination.r != RED.r)
				{
					currentIlumination.r--;
					dayChange = false;
				}
				if (currentIlumination.g != RED.g)
				{
					currentIlumination.g--;
					dayChange = false;
				}
				if (dayChange)
				{
					currentDayMoment = DAY_MOMENT::NOON;
				}
				break;
			case global_ilumination::DAY_MOMENT::NOON:
				if (currentIlumination.b != DARKBLUE.b)
				{
					currentIlumination.b++;
					dayChange = false;
				}
				if (currentIlumination.r != DARKBLUE.r)
				{
					currentIlumination.r--;
					dayChange = false;
				}
				if (currentIlumination.g != DARKBLUE.g)
				{
					currentIlumination.g++;
					dayChange = false;
				}
				if (dayChange)
				{
					currentDayMoment = DAY_MOMENT::NIGHT;
				}
				break;
			case global_ilumination::DAY_MOMENT::NIGHT:
				if (currentIlumination.b != YELLOW.b)
				{
					currentIlumination.b--;
					dayChange = false;
				}
				if (currentIlumination.r != YELLOW.r)
				{
					currentIlumination.r++;
					dayChange = false;
				}
				if (currentIlumination.g != YELLOW.g)
				{
					currentIlumination.g++;
					dayChange = false;
				}
				if (dayChange)
				{
					currentDayMoment = DAY_MOMENT::MORNING;
				}
				break;
			}
			iluminationFrame = 0;
		}
	}
}
