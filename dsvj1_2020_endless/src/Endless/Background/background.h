#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "raylib.h"

class Background
{
private:
	Texture2D* texture;
	Vector2 position;
	Rectangle rectangle;
	float speed;
public:
	Background();
	Background(Texture2D* tex, Vector2 pos, float spd);
	~Background();
	void update();
	void draw();
	Texture2D* getTexture();
	void setTexture(Texture2D* tex);
};

#endif
