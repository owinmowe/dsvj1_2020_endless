#include "background.h"
#include "Endless/ScreenConfig/screen_config.h"
#include "Endless/Draw/post_processing.h"

Background::Background()
{
	texture = nullptr;
	position = { 0, 0 };
	rectangle = { 0, 0, 0, 0 };
	speed = 0;
}
Background::Background(Texture2D* tex, Vector2 pos, float spd)
{
	texture = tex;
	texture->width = screenConfig::SCREEN_WIDTH * 3;
	texture->height = screenConfig::SCREEN_HEIGHT;
	position = pos;
	rectangle = { 0, 0, static_cast<float>(texture->width/3) , static_cast<float>(texture->height) };
	speed = spd;
}
Background::~Background()
{

}
void Background::update()
{
	rectangle.x += static_cast<int>(speed * GetFrameTime() * screenConfig::SCREEN_WIDTH);
	if(rectangle.x + rectangle.width > texture->width)
	{
		rectangle.x = rectangle.x + rectangle.width - texture->width;
	}
}
void Background::draw()
{
	DrawTextureRec(*texture, rectangle, position, global_ilumination::getCurrentIluminationColor());
}
Texture2D* Background::getTexture()
{
	return texture;
}
void Background::setTexture(Texture2D* tex)
{
	texture = tex;
}