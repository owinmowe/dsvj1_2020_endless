#ifndef ENDLESS_H
#define ENDLESS_H

#include "raylib.h"

#include "Scenes/scene_manager.h"
#include "ScreenConfig/screen_config.h"
#include "Input/Input.h"
#include "Update/update.h"
#include "Draw/draw.h"
#include "Audio/audio.h"
#include "EventSystem/event_system.h"

class Endless {

private:
	void input();
	void update();
	void draw();

public:
	Endless();
	~Endless();
	void play();

};

#endif
