#ifndef EVENT_SYSTEM
#define EVENT_SYSTEM


namespace event_system
{

	enum class EVENT_ID { MOVE_MENU, SELECT_MENU, HIT, JUMP, DASH, ROCK_BREAK, MOVE, ITEM_GET };

	class EventSystem
	{
	private:
		bool moveMenuBool;
		bool selectMenuBool;
		bool hitEventBool;
		bool jumpEventBool;
		bool dashEventBool;
		bool moveEventBool;
		bool itemGetEventBool;
		bool rockBreakBool;
	public:
		EventSystem();
		~EventSystem();
		void setEvent(EVENT_ID eventNumber);
		bool checkEvent(EVENT_ID eventNumber);
		void ResetEvents();
	};

	extern EventSystem* externalEventSystem;

}

#endif