#include "event_system.h"

namespace event_system
{

	EventSystem* externalEventSystem;

	EventSystem::EventSystem()
	{
		moveMenuBool = false;
		selectMenuBool = false;
		moveEventBool = false;
		hitEventBool = false;
		jumpEventBool = false;
		dashEventBool = false;
		itemGetEventBool = false;
		rockBreakBool = false;
	}
	EventSystem::~EventSystem()
	{

	}
	void EventSystem::setEvent(EVENT_ID eventNumber)
	{
		switch (eventNumber)
		{
		case event_system::EVENT_ID::MOVE_MENU:
			moveMenuBool = true;
			break;
		case event_system::EVENT_ID::SELECT_MENU:
			selectMenuBool = true;
			break;
		case event_system::EVENT_ID::HIT:
			hitEventBool = true;
			break;
		case event_system::EVENT_ID::JUMP:
			jumpEventBool = true;
			break;
		case event_system::EVENT_ID::DASH:
			dashEventBool = true;
			break;
		case event_system::EVENT_ID::MOVE:
			moveEventBool = true;
			break;
		case event_system::EVENT_ID::ITEM_GET:
			itemGetEventBool = true;
			break;
		case event_system::EVENT_ID::ROCK_BREAK:
			rockBreakBool = true;
			break;
		default:
			break;
		}
	}
	bool EventSystem::checkEvent(EVENT_ID eventNumber)
	{
		switch (eventNumber)
		{
		case event_system::EVENT_ID::MOVE_MENU:
			return moveMenuBool;
			break;
		case event_system::EVENT_ID::SELECT_MENU:
			return selectMenuBool;
			break;
		case event_system::EVENT_ID::HIT:
			return hitEventBool;
			break;
		case event_system::EVENT_ID::JUMP:
			return jumpEventBool;
			break;
		case event_system::EVENT_ID::ITEM_GET:
			return itemGetEventBool;
			break;
		case event_system::EVENT_ID::DASH:
			return dashEventBool;
			break;
		case event_system::EVENT_ID::MOVE:
			return moveEventBool;
			break;
		case event_system::EVENT_ID::ROCK_BREAK:
			return rockBreakBool;
			break;
		default:
			return false;
			break;
		}
	}
	void EventSystem::ResetEvents()
	{
		moveMenuBool = false;
		selectMenuBool = false;
		hitEventBool = false;
		jumpEventBool = false;
		dashEventBool = false;
		moveEventBool = false;
		itemGetEventBool = false;
		rockBreakBool = false;
	}
}